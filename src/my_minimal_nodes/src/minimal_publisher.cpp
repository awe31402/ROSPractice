#include <ros/ros.h>
#include <std_msgs/Float64.h>

#define NODE_NAME "minimal_publisher"
#define TOPIC_NAME "topic1"

#define LOOP_RATE 10
int main(int argc, char** argv)
{
    ros::init(argc, argv, NODE_NAME);
    ros::NodeHandle n;

    ros::Publisher my_publisher_object = 
        n.advertise<std_msgs::Float64> (TOPIC_NAME, 1);
    
    ros::Rate napRate(LOOP_RATE);

    std_msgs::Float64 input_float;
    input_float.data = 0.001;

    while (ros::ok()) {
        input_float.data += input_float.data;
        my_publisher_object.publish(input_float);
        ros::spinOnce();
        napRate.sleep();
    } 

    return 0;
}
