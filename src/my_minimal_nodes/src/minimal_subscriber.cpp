#include <ros/ros.h>
#include <std_msgs/Float64.h>

#define NODE_NAME "minimal_subscriber"
#define LISTEN_TOPIC "topic1"

void myCallback(const std_msgs::Float64& message_holder)
{
    ROS_INFO_STREAM("Received data: " << message_holder.data << std::endl);
}

int main(int argc, char ** argv)
{
    ros::init(argc, argv, NODE_NAME);
    ros::NodeHandle n;

    ros::Subscriber my_subscriber_object =
        n.subscribe(LISTEN_TOPIC, 1, myCallback);
    
    ros::spin();
    return 0;
}
