#include <ros/ros.h>
#include <example_ros_msg/VecOfDoubles.h>

#define NODE_NAME "vector_subscriber"
#define VEC_TOPIC "vec_topic"
using namespace std;

void callback(const example_ros_msg::VecOfDoubles &message_holder)
{
    vector<double> vec_of_dbl = message_holder.dbl_vec;

    int size = vec_of_dbl.size();

    for (int i = 0; i < size; i++)
        ROS_INFO_STREAM("vec_dbl[" << i << "] = "
            << vec_of_dbl[i] << endl);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, NODE_NAME);
    ros::NodeHandle nh;

    ros::Subscriber sub = nh.subscribe(VEC_TOPIC, 1, callback);

    ros::spin();
    return 0;
}
