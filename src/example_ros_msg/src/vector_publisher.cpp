#include <ros/ros.h>
#include <example_ros_msg/VecOfDoubles.h>

#define NODE_NAME "vector_publisher"
#define TOPIC_NAME "vec_topic"
int main(int argc, char** argv)
{
    double counter = 0.0;
    ros::init(argc, argv, NODE_NAME);

    ros::NodeHandle nh;
    
    ros::Publisher pub = nh.advertise<example_ros_msg::VecOfDoubles>
        (TOPIC_NAME, 1);

    ros::Rate naptime(3.0);

    example_ros_msg::VecOfDoubles message;

    message.dbl_vec.resize(3);

    message.dbl_vec[0] = 1.414;
    message.dbl_vec[1] = 2.718;
    message.dbl_vec[2] = 3.1416;

    while (ros::ok()) {
        counter += 1.0;
        message.dbl_vec.push_back(counter);
        pub.publish(message);
        naptime.sleep();       
    }
    return 0;
}
