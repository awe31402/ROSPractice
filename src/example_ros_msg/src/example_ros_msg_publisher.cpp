#include <ros/ros.h>
#include <example_ros_msg/ExampleMsg.h>
#include <math.h>

#define NODE_NAME "example_ros_msg_publisher"

int main(int argc, char** argv)
{
    double sqrt_arg;

    ros::init(argc, argv, NODE_NAME);
    ros::NodeHandle n;

    ros::Publisher example_msg_publisher =
        n.advertise<example_ros_msg::ExampleMsg>("example_topic", 1);

    example_ros_msg::ExampleMsg new_msg;

    ros::Rate naptime(1.0);

    new_msg.header.stamp = ros::Time::now();
    new_msg.header.seq = 0;
    new_msg.header.frame_id = "base_frame";

    new_msg.demo_int = 1;
    new_msg.demo_double = 100.0;

    while (ros::ok()) {
        ++new_msg.header.seq;
        new_msg.header.stamp = ros::Time::now();
        new_msg.demo_int *= 2.0;
        sqrt_arg = new_msg.demo_double;
        new_msg.demo_double = sqrt(sqrt_arg);
        example_msg_publisher.publish(new_msg);
        naptime.sleep();
    }
    return 0;
}
