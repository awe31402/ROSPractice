#include <ros/ros.h>
#include <example_ros_srv/ExampleServiceMsg.h>
#include <iostream>
#include <string>

using namespace std;
#define NODE_NAME "example_ros_client"
#define TOPIC_NAME "lookup_by_name"

int main(int argc, char** argv)
{
    ros::init(argc, argv, NODE_NAME);
    ros::NodeHandle nh;
    ros::ServiceClient client = nh.serviceClient
        <example_ros_srv::ExampleServiceMsg>(TOPIC_NAME);
    
    example_ros_srv::ExampleServiceMsg srv;
    
    bool on_the_list = false;
    string in_name;

    while (ros::ok()) {
        cout << "enter name (x to quit): ";
        cin >> in_name;
        if (in_name == "x")
            return 0;
    
        srv.request.name = in_name;
    
        if (client.call(srv)) {
            if (srv.response.on_the_list) {
                cout << srv.request.name << " is known as"
                    << srv.response.nickname << endl;
                cout << "Hes age: "   << srv.response.age << endl;
                cout << "He is a " << ((srv.response.good_guy)?
                    "good" :  "bad") << " guy" << endl;
            } else {
                cout << srv.request.name << " is not on the list"
                    << endl;
            }
        } else {
            ROS_ERROR_STREAM("Error to connect service "
                << TOPIC_NAME << endl);
            
        }
    }
    return 0;
}
