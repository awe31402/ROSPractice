#include <ros/ros.h>
#include <example_ros_srv/ExampleServiceMsg.h>
#include <iostream>
#include <string>
using namespace std;

#define NODE_NAME "example_ros_service"
#define SERVICE_NAME "lookup_by_name"

#define LOG_TAG "[example_ros_service]: "

bool callback(example_ros_srv::ExampleServiceMsgRequest &request,
    example_ros_srv::ExampleServiceMsgResponse &response)
{
    ROS_INFO_STREAM(LOG_TAG << "Callback Activated" << endl);
    string in_name(request.name);
    response.on_the_list = false;

    if (0 == in_name.compare("Bob")) {
        ROS_INFO_STREAM("asked about Bob" << endl);
        response.age = 21;
        response.good_guy = false;
        response.on_the_list = true;
        response.nickname = "BobtheTerrible";
    }

    if (0 == in_name.compare("Ted")) {
        ROS_INFO_STREAM("asked about Ted" << endl);
        response.age = 22;
        response.good_guy = true;
        response.on_the_list = true;
        response.nickname = "Tedthegood";
    }

    return true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, NODE_NAME);
    ros::NodeHandle nh;
    ros::ServiceServer srv = nh.advertiseService(SERVICE_NAME,
            callback);

    ros::spin();
    return 0;
}
