#include <ros/ros.h>
#include "example_ros_class.h"

#define NODE_NAME "example_ros_class"

int main(int argc, char** argv)
{
    ros::init(argc, argv, NODE_NAME);
    ros::NodeHandle nh;
    ExampleRosClass obj(nh);
    ros::spin();
    return 0;
}
