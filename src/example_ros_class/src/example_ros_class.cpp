#include "example_ros_class.h"
using namespace std;

string ExampleRosClass::PUBLISH_TOPIC = "example_class_output_topic";
string ExampleRosClass::SUBSCRIBE_TOPIC = "example_class_input_topic";
string ExampleRosClass::SERVICE_NAME = "example_class_service";

ExampleRosClass::ExampleRosClass(ros::NodeHandle nh)
{
    ROS_INFO_STREAM("[ExampleRosClass] Init" << endl);
    m_nh = nh;
    m_val = 0.0;
    init_publisher();
    init_subscriber();
    init_service();
}

void ExampleRosClass::init_publisher()
{
    ROS_INFO_STREAM("[ExampleRosClass] Init publisher" << endl);
    m_pub = m_nh.advertise<std_msgs::Float32>(PUBLISH_TOPIC, 1);
}

void ExampleRosClass::init_subscriber()
{
    ROS_INFO_STREAM("[ExampleRosClass] Init subscriber" << endl);
    m_sub = m_nh.subscribe(SUBSCRIBE_TOPIC, 1,
                &ExampleRosClass::subscriber_callback, this);
}

void ExampleRosClass::subscriber_callback(const std_msgs::Float32 &message_holder)
{
    ROS_INFO_STREAM("[ExampleRosClass] subscriber_callback" << endl);
    std_msgs::Float32 out_msg;

    m_val += message_holder.data;

    out_msg.data = m_val; 
    m_pub.publish(out_msg);
}

void ExampleRosClass::init_service()
{
    ROS_INFO_STREAM("[ExampleRosClass] Init service" << endl);
    m_srv = m_nh.advertiseService(SERVICE_NAME,
                &ExampleRosClass::service_callback, this);
}

bool ExampleRosClass::service_callback(std_srvs::TriggerRequest &request,
    std_srvs::TriggerResponse & response)
{
    ROS_INFO_STREAM("[ExampleRosClass] service callback" << endl);
    response.success = true;
    response.message = "herer's the message";
    return true;
}
