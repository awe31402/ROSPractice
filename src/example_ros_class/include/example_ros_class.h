#ifndef EXAMPLE_ROS_CALSS_H
#define EXAMPLE_ROS_CALSS_H

#include <math.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Bool.h>
#include <std_srvs/Trigger.h>

class ExampleRosClass {
public:
    ExampleRosClass(ros::NodeHandle nh);
    static std::string PUBLISH_TOPIC;
    static std::string SUBSCRIBE_TOPIC;
    static std::string SERVICE_NAME;
private:
    float m_val;
    void init_publisher();
    void init_subscriber();
    void init_service();
    void subscriber_callback(const std_msgs::Float32 &message_holder);
    bool service_callback(std_srvs::TriggerRequest &request, std_srvs::TriggerResponse &response);
    ros::NodeHandle m_nh;
    ros::Publisher m_pub;
    ros::Subscriber m_sub;
    ros::ServiceServer m_srv;
};
#endif
