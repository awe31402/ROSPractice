#include <ros/ros.h>
#include <std_msgs/Float64.h>

#define NODE_NAME "controller_node"

std_msgs::Float64 g_velocity;
std_msgs::Float64 g_force;
std_msgs::Float64 vel_cmd;

void velocityCallback(const std_msgs::Float64 &message_holder)
{
    g_velocity.data = message_holder.data;
    ROS_INFO_STREAM("[" << NODE_NAME << "] Received velocyty data: "
        << message_holder.data << std::endl);
}

void velCmdCallback(const std_msgs::Float64 &message_holder)
{
    vel_cmd.data = message_holder.data;
    ROS_INFO_STREAM("[" << NODE_NAME << "] Received velocyty command data: "
        << message_holder.data << std::endl);
}

int main(int argc, char** argv)
{
    double dt = 0.1;
    double K = 1.0;
    
    ros::init(argc, argv, NODE_NAME);
    ros::NodeHandle nh;
    
    ros::Subscriber velocitySubscriber =
        nh.subscribe("velocity", 1, velocityCallback);
    ros::Subscriber velCmdSubscriber =
        nh.subscribe("vel_cmd", 1, velCmdCallback);

    ros::Publisher forceCmdPublisher =
        nh.advertise<std_msgs::Float64>("force_cmd", 1);

    ros::Rate naptime(1.0 /dt);
    g_velocity.data = 0.0;
    g_force.data = 0.0;
    vel_cmd.data = 0.0;

    while (ros::ok()) {
        double vel_err = vel_cmd.data - g_velocity.data;
        g_force.data = K * vel_err;

        forceCmdPublisher.publish(g_force);
        ros::spinOnce();
        naptime.sleep();
    }
    return 0;
}
