#include <ros/ros.h>
#include <std_msgs/Float64.h>

#define NODE_NAME "simulator_node"

std_msgs::Float64 g_force;
std_msgs::Float64 g_vel;

void myCallback(const std_msgs::Float64 &message_holder)
{
    ROS_INFO_STREAM("[" << NODE_NAME << "]: Received Data "
        << message_holder.data << std::endl);
    g_force.data = message_holder.data;
}

int main(int argc, char**argv)
{
    double dt = 0.01;
    double mass = 1.0;
    double samplingRate = 1.0 / dt;

    ros::init(argc, argv, NODE_NAME);

    ros::NodeHandle nh;

    ros::Publisher myPublisher =
        nh.advertise<std_msgs::Float64>("velocity", 1);
    ros::Subscriber mySubscriber =
        nh.subscribe("force_cmd", 1, myCallback);

    ros::Rate naptime(samplingRate);
    g_force.data = 0.0;
    g_vel.data = 0.0;

    ROS_INFO_STREAM("[" << NODE_NAME << "]: Ready\n");
    while (ros::ok()) {
        g_vel.data += g_force.data / mass * dt;
        myPublisher.publish(g_vel);
        ros::spinOnce();
        naptime.sleep();
    }
    return 0;
}
