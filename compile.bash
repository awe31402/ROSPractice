#!/bin/bash

if [ "$1" == "clean" ] || [ "$1" == "rebuild" ]
then
    echo "Removing build direcories"
    rm -rf build devel
fi

if [ "$1" == "rebuild" ]
then
    echo "init work space"
    catkin_init_workspace
    unlink CMakeLists.txt
fi

if [ "$1" == "clean" ]
then
    exit 0
fi

echo "Start building"
catkin_make
